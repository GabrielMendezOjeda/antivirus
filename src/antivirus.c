/**
 * This file defines a antivirus.
 *
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image and the counters virusEaten and delayEaten.
 */
void startAntivirus(Actor antivirus) {
	setImageFile(antivirus, "antivirus.png");
	setGlobalByte(antivirus, "virusEaten", 0);
	setGlobalByte(antivirus, "delayEaten", 0);
}

/**
 * Check if a keyboard control key has been pressed. If so,
 * react accordingly.
 */
void checkKeypress(Actor antivirus) {
	if (isKeyDown("left")) {
		turn(antivirus, -4);
	}
	if (isKeyDown("right")) {
		turn(antivirus, 4);
	}
}

/**
 * Check if we have encountered a virus. If so, eat it. If not,
 * it does nothing. When there are no bacteria left, we win.
 */
void lookForVirus(Actor antivirus) {
	if (isTouching(antivirus, "virus")) {
		removeTouching(antivirus, "virus");
		playSound("au.wav");
		setImageFile(antivirus, "antivirus-1.png");
		int virusEaten = getGlobalByte(antivirus, "virusEaten") + 1;
		setGlobalByte(antivirus, "virusEaten", virusEaten);
	}
}

/**
 * After eating, the image of the antivirus changes, going through
 * the sequence image-1, image-2, image-3 and finally image
 */
void switchImage(Actor antivirus) {
	if (strcmp(getImageFile(antivirus),"antivirus-1.png") == 0) {
		int delayEaten = getGlobalByte(antivirus, "delayEaten");
		if (delayEaten == 3) {
			setImageFile(antivirus, "antivirus-2.png");
			setGlobalByte(antivirus, "delayEaten", 0);
		} else {
			setGlobalByte(antivirus, "delayEaten", delayEaten+1);
		}
	} else 	if (strcmp(getImageFile(antivirus),"antivirus-2.png") == 0) {
		int delayEaten = getGlobalByte(antivirus, "delayEaten");
		if (delayEaten == 3) {
			setImageFile(antivirus, "antivirus-3.png");
			setGlobalByte(antivirus, "delayEaten", 0);
		} else {
			setGlobalByte(antivirus, "delayEaten", delayEaten+1);
		}
	} else 	if (strcmp(getImageFile(antivirus),"antivirus-3.png") == 0) {
		int delayEaten = getGlobalByte(antivirus, "delayEaten");
		if (delayEaten == 3) {
			setImageFile(antivirus, "antivirus.png");
			setGlobalByte(antivirus, "delayEaten", 0);
			if (getGlobalByte(antivirus, "virusEaten") == 3) {
				playSound("fanfare.wav");
				stopScenario();
			}
		} else {
			setGlobalByte(antivirus, "delayEaten", delayEaten+1);
		}
	}
}

/**
 * Act - do whatever the antivirus wants to do. This function
 * is called whenever the 'Act' or 'Run' button gets pressed
 * in the environment.
 */
void actAntivirus(Actor antivirus) {
	if (strcmp(getImageFile(antivirus),"antivirus.png") == 0) {
	    checkKeypress(antivirus);
		move(antivirus, 5);
		lookForVirus(antivirus);
	} else {
		switchImage(antivirus);
	}
}

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "computer.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_computer_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startComputer(object);
}

JNIEXPORT void JNICALL Java_computer_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actComputer(object);
}


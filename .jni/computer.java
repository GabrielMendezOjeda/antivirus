/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

import greenfoot.*;

public class computer extends Actor {
	
	public computer() {
		start();
	}

    public void start(){
        start_();
    }
    private native void start_();

	public void act(){
        act_();
    }
    private native void act_();


    static {
        System.load(new java.io.File(".jni", "computer_jni.so").getAbsolutePath());
    }
}